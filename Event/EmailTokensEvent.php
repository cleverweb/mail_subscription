<?php

namespace Cleverweb\MailsBundle\Event;

use Cleverweb\MailsBundle\Entity\Mailing;
use Perfico\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class EmailTokensEvent extends Event {

    /**
     * @var User|null
     */
    protected $user;

    /**
     * @var array
     */
    protected $tokens;

    /**
     * @var Mailing $mailing
     */
    protected $mailing;

    /**
     * @param $user
     * @param Mailing $mailing
     */
    public function __construct($user, $mailing){
        $this->user = $user;
        $this->mailing = $mailing;
        $this->tokens = [];
    }

    /**
     * @return Mailing
     */
    public function getMailing() {
        return $this->mailing;
    }

    /**
     * @param Mailing $mailing
     */
    public function setMailing(Mailing $mailing) {
        $this->mailing = $mailing;
    }

    /**
     * return result tokens
     *
     * @return array
     */
    public function getTokens() {
        return $this->tokens;
    }

    /**
     * set result tokens
     *
     * @param array $tokens
     */
    public function setTokens($tokens) {
        $this->tokens = $tokens;
    }

    /**
     * If you need to add just token name, don't specify the value.
     *
     * @param $name
     * @param $value
     */
    public function addToken($name, $value = true) {
        $this->tokens[$name] = $value;
    }

    /**
     * @return mixed
     */
    public function getUser(){
        return $this->user;
    }
} 