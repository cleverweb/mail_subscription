<?php

namespace Cleverweb\MailsBundle\Event;

use Cleverweb\MailsBundle\Entity\Mailing;
use Symfony\Component\EventDispatcher\Event;

class BeforeInsertUsersEvent extends Event {

    protected $users;

    /**
     * @var Mailing $mailing
     */
    protected $mailing;

    public function __construct( $users , Mailing $mailing){
        $this->users = $users;
        $this->mailing = $mailing;
    }

    /**
     * @return Mailing
     */
    public function getMailing() {
        return $this->mailing;
    }

    /**
     * @param Mailing $mailing
     */
    public function setMailing(Mailing $mailing) {
        $this->mailing = $mailing;
    }

    public function getUsers(){
        return $this->users;
    }
} 