<?php

namespace Cleverweb\MailsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('cleverweb_mails');

        $rootNode
            ->children()
            ->arrayNode('host')
            ->addDefaultsIfNotSet()
            ->canBeUnset()
            ->children()
            ->scalarNode('address')->defaultValue('localhost')->end()
            ->end()
            ->end()
            ->arrayNode('mail')
            ->addDefaultsIfNotSet()
            ->canBeUnset()
            ->children()
            ->scalarNode('template')->defaultValue('CleverwebMailsBundle:Mail:mail_layout.html.twig')->end()
            ->end()
            ->end()
            ->arrayNode('user')
            ->addDefaultsIfNotSet()
            ->canBeUnset()
            ->children()
            ->scalarNode('model')->defaultValue('FOS\\UserBundle\\Model\\User')->end()
            ->arrayNode('provider')
            ->addDefaultsIfNotSet()
            ->canBeUnset()
            ->children()
            ->scalarNode('get_subscribed_users')->defaultValue(null)->end()
            ->scalarNode('subscribed_field')->defaultValue(null)->end()
            ->scalarNode('email_field')->defaultValue('email')->end()
            ->scalarNode('full_name_field')->defaultValue('username')->end()
            ->end()
            ->end()
            ->arrayNode('filters')
            ->canBeUnset()
            ->requiresAtLeastOneElement()
            ->prototype('scalar')
            ->end()
            ->end()
            ->end()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
