CleverWeb Mails
===============

CleverwebMailsBundle provides simple Mailing system for Symfony2.

Installation:
=============

At first, check that you have installed next bundles:
```
"sonata-project/admin-bundle": "2.2.*"
"egeloen/ckeditor-bundle": "dev-master"
```

Then add repository and bundle to composer.json:

```Javascript
"require": {
    ...,
    "cleverweb/mails-bundle": "1.0.*"
},
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:cleverweb/mail_subscription.git"
    }
],
```


Then run installation:

```bash
php composer.phar update cleverweb/mails-bundle
```

And add bundle in AppKernel.php:

```php
new Cleverweb\MailsBundle\CleverwebMailsBundle(),
```

Configuration:
==============
in app/config.yml you have to provide model for Users in your project, and ensure you have next getters:
**getEmail**, **getFullName**.

If you need to have ability users to unsubscribe, you have to specify **subscribed_field** of your user model.
This must be text nullable field.
For example, description of field in entity may be:
```php
    /**
     * @ORM\Column(name="subscribed", type="string", length=100, nullable=true)
     */
    protected $subscribed;
```

And usually your config will be next:

```Yaml
cleverweb_mails:
    host:
        address: http://www.mysitename.ru
    user:
        model: Cleverweb\UserBundle\Entity\User
        provider:
            email_field: email
            full_name_field: username
            subscribed_field: subscribed
```

You can specify your own user filters. These filters should be added to user repository and these methods should return query builder for users:
```Yaml
cleverweb_mails:
    host:
        address: http://www.mysitename.ru
    user:
        model: Cleverweb\UserBundle\Entity\User
        provider:
            email_field: email
            full_name_field: username
            subscribed_field: subscribed
        filters:
            kindly_guys: getKindly
            bad_buys: getBad
```

You can use filter ```All: ~``` for all users.

Note that name of filters will be translated with domain "CleverwebMailsBundle" and will has prefix "mailing.filter.".
  For example "mailing.filter.kindly_guys".

Also bundle provide these names for JMSTranslationBundle.


You can add template for letter.
In template use {{ data|raw }} for replacing letter content.
```Yaml
cleverweb_mails:
    host:
        address: http://www.mysitename.ru
    user:
        model: Cleverweb\UserBundle\Entity\User
        provider:
            email_field: email
            full_name_field: username
            subscribed_field: subscribed
        filters:
            kindly_guys: getKindly
            bad_buys: getBad
    mail:
        template: '::mail_template.html.twig'
```

Also you can simply use User from FOSUserBundle. This config will be next:
```Yaml
cleverweb_mails:
    host:
        address: http://www.mysitename.ru
    user:
        model: FOS\UserBundle\Model\User
```

Note: host.address is used for creating unsubscribe link.


## Routing
You have to add in your routing.yml:
```yml
cleverweb_mails:
    resource: "@CleverwebMailsBundle/Controller/"
    type: annotation
```


## Integrate with SonataAdminBundle
In sonata config find blocks configuration and add line:
```yml
sonata_block:
    ...
    sonata.block.service.queue_manager:
```

And also find dashboard configuration and add line for queue_manager block:

```yml
sonata_admin:
    ...
    dashboard:
        blocks:
            ...
            - { position: left, type: sonata.block.service.queue_manager }
```

And at the end, update doctrine and clear cache:

```
    php app/console doctrine:schema:update --force
    php app/console ca:cl
```


## Events
####Dynamic variables

To dynamically change available variables in email you can use event CleverwebMailsEvents::ON_PREPARE_LETTER_HTML, which executes when system is preparing letter.
You need to define subscriber in your bundle, that will change event object by adding
 new tokens with name and value:

```php

    static public function getSubscribedEvents()
    {
        return array(
            CleverwebMailsEvents::ON_PREPARE_LETTER_HTML => 'onPrepareLetterHtml'
        );
    }

    public function onPrepareLetterHtml(EmailTokensEvent $event) {
            /**
             * @var User $user
             */
            $user = $event->getUser();

            $html = $this->twig->render(
                'RealtorBundle:Mailer:activateLink.html.twig',
                array(
                    'token' => $user->getConfirmationToken(),
                    'host' => $this->host
                )
            );

            $event->addToken('realtor_token', $html);
        }
```

Also you should subscribe it to additional event CleverwebMailsEvents::ON_GET_LETTER_VARIABLES and 
return just name of this token, like:

```php
    $event->addToken('realtor_token', $html);
```

It needs for generating help of letter editing form.  

#### Change data before inserting user in queue

You can change some data before inserting user in queue by subscribing to event CleverwebMailsEvents::BEFORE_USER_INSERT_IN_QUEUE:


```php

    static public function getSubscribedEvents()
    {
        return array(
            CleverwebMailsEvents::BEFORE_USER_INSERT_IN_QUEUE => 'beforeUsersInsertInQueue'
        );
    }
    
    public function beforeUsersInsertInQueue( BeforeInsertUsersEvent $event) {
        /**
         * @var User[] $users
         */
        $users = $event->getUsers();
    
        foreach($users as $user){
            if ($this->userManager->isRealtor($user)) {
                $user->setConfirmationToken($this->userManager->generateToken(Rand()));
                $this->userManager->updateUser($user);
            }
        }
    }

```


Usage
=====

Bundle integrates with Sonata Dashboard and you can use dashboard to find all control items.
Standard Sonata dashboard route is:
```
/admin/dashboard
```

To send emails, you have to run console command:

```bash
app/console cleverweb:mails:send
```


Parameters: delay in milliseconds and maximum count of emails to send.
Default values: delay = 500, count = 20.
For example:
```bash
app/console cleverweb:mails:send --delay=10000 --count=10
```

For example cron rule will be (for every minute tick):
```
*       *       *       *       *       /usr/bin/php /PATH_TO_YOUR_PROJECT/app/console cleverweb:mails:send --delay=500 --count=100
```

If you had setup JMS Translation Bundle you should add extractor to jms_translation config:
```Yml
    extractors: [cleverweb_mails]
```



#### First mailing:

1. In dashboard, create new Letter.
In letter text use {{name}} for replacing user name and {{unsubscribe}} for replacing unsubscribe link.
2. Create new mailing and enable it.
3. In dashboard you can see mailings status.



#### Known issues:

You can add sonata_admin extractor to JMS Translation config for full extraction of texts.
But you have to know that ckeditor bundle has some code that broke extraction process.
In that reason we don't add sonata_admin now and catch messages from it manually.
In future we change ckeditor to something another or fork this bundle.

#### TO-DO:

Add BCC for mass sending.
Extractor must provide all fields for extraction.