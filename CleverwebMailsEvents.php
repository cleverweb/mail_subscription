<?php

namespace Cleverweb\MailsBundle;

final class CleverwebMailsEvents
{
    /** Listners can add tokens with name and value */
    const ON_PREPARE_LETTER_HTML = 'cleverveb.mailer.on.prepare.letter.html';

    /** Listners can add token names without values */
    const ON_GET_LETTER_VARIABLES = 'cleverveb.mailer.on.get.letter.variables';

    /** Listener can be used to change some user related fields before inserting user in queue */
    const BEFORE_USER_INSERT_IN_QUEUE = 'cleverweb.mailer.before.user.insert.in.queue';
}
