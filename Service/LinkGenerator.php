<?php


namespace Cleverweb\MailsBundle\Service;


use Cleverweb\MailsBundle\Entity\Mailing;
use Doctrine\ORM\EntityManager;

class LinkGenerator {

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Generates link contains
     * Letter Id, Mailing Id, Timestamp
     *
     * @param Mailing $mailing
     * @return string
     */
    public function generateLinkData(Mailing $mailing, $userId)
    {
        $time = new \DateTime();
        $parts = [
            $userId,
            $mailing->getLetter()->getId(),
            $mailing->getId(),
            $time->getTimestamp()
        ];

        return base64_encode(implode(".",$parts));
    }

    public function decodeLinkData($data)
    {
        $parts = explode('.', base64_decode($data));

        $letter = $this->em->getRepository('CleverwebMailsBundle:Letter')->find((int) $parts[1]);
        $mailing = $this->em->getRepository('CleverwebMailsBundle:Mailing')->find((int) $parts[2]);
        if ($mailing->getLetter() !== $letter) {
            $mailing = null;
        }

        return [
            'mailing' => $mailing,
            'letter' => $letter,
            'userId' => (int) $parts[0],
            'timestamp' => (int) $parts[3]
        ];
    }


}