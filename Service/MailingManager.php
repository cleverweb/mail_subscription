<?php


namespace Cleverweb\MailsBundle\Service;


use Cleverweb\MailsBundle\CleverwebMailsEvents;
use Cleverweb\MailsBundle\Entity\Mailing;
use Cleverweb\MailsBundle\Entity\Queue;
use Cleverweb\MailsBundle\Event\BeforeInsertUsersEvent;
use Cleverweb\MailsBundle\Event\EmailTokensEvent;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Twig_Environment as Environment;

class MailingManager
{

    /** @var  EntityManager */
    private $em;

    private $managerSettings;

    /** @var EntityRepository */
    private $userRepository;

    /** @var  \Swift_Mailer */
    private $mailer;

    /** @var  Environment */
    private $twig;

    private $subscribedGetter = null;
    private $subscribedSetter;
    private $emailGetter;
    private $fullNameGetter = null;
    private $host;

    private $mailSettings;

    /**
     * @var LinkGenerator
     */
    private $linkGenerator;

    /**
     * @var EventDispatcher $eventDispatcher
     */
    private $eventDispatcher;

    public function __construct(
        DoctrineRegistry $doctrine,
        $mailer,
        Environment $twig,
        LinkGenerator $linkGenerator,
        $configHost,
        $configUser,
        $configMail,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->twig = $twig;
        $this->em = $doctrine->getManager();
        $this->linkGenerator = $linkGenerator;
        $this->managerSettings = $configUser;
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $doctrine
            ->getManagerForClass($this->managerSettings['model'])
            ->getRepository($this->managerSettings['model']);
        $this->mailer = $mailer;

        $this->mailSettings = $configMail;

        if ($this->managerSettings['provider']['subscribed_field'] != null) {
            $this->subscribedGetter = 'get' . Container::camelize(
                    $this->managerSettings['provider']['subscribed_field']
                );
            $this->subscribedSetter = 'set' . Container::camelize(
                    $this->managerSettings['provider']['subscribed_field']
                );
        }

        $this->emailGetter = 'get' . Container::camelize($this->managerSettings['provider']['email_field']);
        $this->fullNameGetter = 'get' . Container::camelize($this->managerSettings['provider']['full_name_field']);

        $this->host = $configHost['address'];
    }

    protected function getUserListQueryBuilder($filterName)
    {
        if (!empty($filterName)) {
            $method = $this->managerSettings['filters'][$filterName];

            return $this->userRepository->$method();
        } else {
            return $this->userRepository->createQueryBuilder('u');
        }
    }

    public function createQueue(Mailing $mailing)
    {
        $users = $this->getUserListQueryBuilder($mailing->getFilter())->getQuery()->getResult();

        $subscribedGetter = $this->subscribedGetter;
        $subscribedSetter = $this->subscribedSetter;
        $emailGetter = $this->emailGetter;

        $event = new BeforeInsertUsersEvent($users, $mailing);

        $this->eventDispatcher->dispatch(CleverwebMailsEvents::BEFORE_USER_INSERT_IN_QUEUE, $event);

        foreach ($users as $user) {
            if ($this->subscribedGetter != null) {
                if ($user->$subscribedGetter() === '') {
                    continue;
                }
                if ($user->$subscribedGetter() === null) {
                    $code = mt_rand();
                    $user->$subscribedSetter($code);
                }
                $queue = new Queue();
                $queue->setMailing($mailing);
                $queue->setEmail($user->$emailGetter());
                $queue->setUserId($user->getId());

                $this->em->persist($queue);
            }
        }
        $mailing->setQueueCreated(true);
        $this->em->flush();
    }

    /**
     * Get queue items
     *
     * @param $maxCount
     * @return Queue[]
     */
    public function getQueueItems($maxCount)
    {
        return $this->em->getRepository('CleverwebMailsBundle:Queue')->getQueueItemsForSend($maxCount);
    }

    /**
     * Queue count to send
     *
     * @return integer
     */
    public function getQueueItemsCount()
    {
        return $this->em->getRepository('CleverwebMailsBundle:Queue')->getQueueItemsForSendCount();
    }


    public function sendQueueItem(Queue $queue)
    {
        $now = new \DateTime();

        $letter = $queue->getMailing()->getLetter();
        $html = $this->prepareLetterHtml($queue->getMailing(), $queue->getUserId());
        $message = \Swift_Message::newInstance()
            ->setSubject($letter->getSubject())
            ->setTo($queue->getEmail())
            ->setBody($html, 'text/html');

        if(preg_match('/(.*)<(.*)>/', $letter->getFrom(), $m)) {
            $message
                ->setFrom(trim($m[2]), trim($m[1]));

        }else {
            $message
                ->setFrom($letter->getFrom());
        }

        $numberOfDelivered = $this->mailer->send($message);
        if ($numberOfDelivered == 1) {
            $queue->setSent(true);
            $queue->setSentAt($now);
            $this->em->flush();

            return array(
                'id' => $queue->getId(),
                'email' => $queue->getEmail(),
                'time' => $queue->getSentAt(),
                'letter' => $letter->getName()
            );
        } else {
            return array('id' => 0, 'email' => $queue->getEmail(), 'time' => $now, 'letter' => $letter->getName());
        }
    }

    private function unsubscribe(UserInterface $user)
    {
        $subscribedGetter = $this->subscribedGetter;
        $subscribedSetter = $this->subscribedSetter;
        if ($subscribedGetter == null) {
            throw new UnsubscribeFieldNotAvailableException();
        }

        $user->$subscribedSetter('');
        $this->em->flush();

        $this->em->getRepository('CleverwebMailsBundle:Queue')->removeUserFromQueue($user);
    }

    /**
     * Unsubscribe user by id
     *
     * @param $userId
     * @return bool
     * @throws UnsubscribeFieldNotAvailableException
     */
    public function unsubscribeUser($userId)
    {
        $user = $this->userRepository->createQueryBuilder('u')
            ->where('u.id = :id')
            ->setParameters(
                array(
                    'id' => $userId
                )
            )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($user !== null) {
            $this->unsubscribe($user);

            return true;
        }

        return false;
    }

    /**
     * Result is boolean:
     *
     *  true - unsubscribed
     *
     *  false - error with codes
     *
     *
     * @param $userId Int
     * @param $userCode Int
     *
     * @throws UnsubscribeFieldNotAvailableException
     * @return mixed
     */
    public function checkAndUnsubscribeUser($userId, $userCode)
    {
        $user = $this->userRepository->createQueryBuilder('u')
            ->where('u.id = :id')
            ->andWhere('u.' . $this->managerSettings['provider']['subscribed_field'] . ' = :code')
            ->setParameters(
                array(
                    'id' => $userId,
                    'code' => $userCode
                )
            )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($user !== null) {
            $this->unsubscribe($user);

            return true;
        }

        return false;
    }

    /**
     * Get all enabled mailings and queue status
     *
     * Return array with keys: mailing, all, sent
     *
     * @return array
     */
    public function getActiveMailings()
    {
        $activeMailings = array();
        $mailings = $this->em->getRepository('CleverwebMailsBundle:Mailing')->getEnabledMailings();
        foreach ($mailings as $mailing) {
            $getStatus = $this->em->getRepository('CleverwebMailsBundle:Queue')->getStatus($mailing);
            $activeMailings [] = array(
                'all' => $getStatus['countAll'],
                'sent' => $getStatus['countSent'],
                'mailing' => $mailing,
                'readCount' => $mailing->getReadCount()
            );
        }

        return $activeMailings;
    }

    /**
     * Clear all queue for Mailing
     *
     * @param int $mailingId
     * @param bool $stopMailing
     * @internal param \Cleverweb\MailsBundle\Entity\Mailing $mailing
     */
    public function clearQueue($mailingId, $stopMailing = false)
    {
        $mailing = $this->em->getRepository('CleverwebMailsBundle:Mailing')->find($mailingId);
        if ($stopMailing) {
            $mailing->setEnabled(false);
        }
        $mailing->setQueueCreated(false);
        foreach ($mailing->getQueues() as $queue) {
            $this->em->remove($queue);
        }
        $this->em->flush();
    }

    /**
     * @param Mailing $mailing
     * @param $userId
     * @return mixed|string
     */
    public function prepareLetterHtml(Mailing $mailing, $userId)
    {
        $letter = $mailing->getLetter();
        $subscribedGetter = $this->subscribedGetter;
        $fullNameGetter = $this->fullNameGetter;
        $user = $this->userRepository->find($userId);

        $html = $letter->getHtml();

        $event = new EmailTokensEvent($user, $mailing);

        $this->eventDispatcher->dispatch(CleverwebMailsEvents::ON_PREPARE_LETTER_HTML, $event);

        $tokens = $event->getTokens() + $this->getStandardTokens($user);

        foreach( $tokens as $tokenName => $tokenValue ) {
            $html = preg_replace('/{{(\s|&nbsp;)*'.$tokenName.'(\s|&nbsp;)*}}/', $tokenValue, $html);
        }

        $html .= $this->twig->render(
            'CleverwebMailsBundle:Subscribe:is_read_image.html.twig',
            [
                'readLinkData' => $this->linkGenerator->generateLinkData($mailing, $user->getId()),
                'host' => $this->host,
            ]
        );

        $html = $this->twig->render(
            $this->mailSettings['template'],
            array(
                'data' => $html
            )
        );

        return $html;
    }

    /**
     * Returns array [ 'token' => 'HTML' ].
     * For $user = null returns the list of available standard tokens: ['token' => true].
     *
     * @param UserInterface $user
     * @return array
     */
    public function getStandardTokens($user)
    {
        if (!$user) {
            return [
                'name' => true,
                'email' => true,
                'id' => true,
                'unsubscribe' => true,
            ];
        }

        $subscribedGetter = $this->subscribedGetter;
        $fullNameGetter = $this->fullNameGetter;

        $unsubscribeLink = '';
        if ($subscribedGetter) {
            $unsubscribeCode = $user->$subscribedGetter();
            $unsubscribeLink = $this->twig->render(
                'CleverwebMailsBundle:Subscribe:deactivate_link.html.twig',
                array(
                    'userCode' => $unsubscribeCode,
                    'userId' => $user->getId(),
                    'host' => $this->host,
                )
            );
        }

        return [
            'name' => $fullNameGetter ? $user->$fullNameGetter() : null,
            'email' => $user->getEmail(),
            'id' => $user->getId(),
            'unsubscribe' => $unsubscribeLink
        ];
    }

    /**
     * @param $mailingId integer
     */
    public function stopQueue($mailingId)
    {
        $mailing = $this->em->getRepository('CleverwebMailsBundle:Mailing')->find($mailingId);
        $mailing->setEnabled(false);
        $this->em->flush();
    }

    /**
     * @param $mailingId
     * @return Mailing
     */
    public function getMailingById($mailingId)
    {
        $mailing = $this->em->getRepository('CleverwebMailsBundle:Mailing')->find($mailingId);

        return $mailing;
    }

}