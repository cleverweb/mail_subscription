<?php


namespace Cleverweb\MailsBundle\Command;


use Cleverweb\MailsBundle\Entity\Queue;
use Cleverweb\MailsBundle\Service\MailingManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('cleverweb:mails:send')
            ->setDescription('Send next mails portion')
            ->addOption(
                'delay',
                null,
                InputOption::VALUE_REQUIRED,
                'Delay between mails in milliseconds, default is 500',
                500
            )
            ->addOption(
                'count',
                null,
                InputOption::VALUE_REQUIRED,
                'Count of mails that will be sent at once with delay between mails, default is 20',
                20
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('translator')->setLocale(
            $this->getContainer()->getParameter('kernel.default_locale')
        );

        $container = $this->getContainer();
        $mailer = $container->get('mailer');
        $spool = $mailer->getTransport()->getSpool();
        $transport = $container->get('swiftmailer.transport.real');

        $delay = $input->getOption('delay');
        $delaySec = floor($delay / 1000);
        $delayNanosec = ($delay - $delaySec * 1000) * 1000000;
        $count = $input->getOption('count');

        /** @var MailingManager $mailManager */
        $mailManager = $this->getContainer()->get('cleverweb.mailing_manager');
        $queueItems = $mailManager->getQueueItems($count);
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($queueItems as $item) {
            try {
                $result = $mailManager->sendQueueItem($item);
                if ($result['id'] == 0) {
                    $output->writeln(
                        '<error> Some error while sending letter ' . $result['letter'] . ' to ' . $result['email'] . ' at ' . $result['time']->format(
                            'Y-m-d H:i:s'
                        ) . ' </error>'
                    );
                    $item->addOneTry();
                } else {
                    $output->writeln(
                        '<info> Sent letter ' . $result['letter'] . ' to ' . $result['email'] . ' at ' . $result['time']->format(
                            'Y-m-d H:i:s'
                        ) . '</info>'
                    );
                }
                $spool->flushQueue($transport);
                time_nanosleep($delaySec, $delayNanosec);
            } catch (\Exception $e) {
                $now = new \DateTime();
                $output->writeln(
                    '<error> Error while sending letter ' . $item->getMailing()->getLetter()->getName(
                    ) . ' to ' . $item->getEmail() . ' at ' . $now->format('Y-m-d H:i:s') . ' </error>'
                );
                if (preg_match("/.*Address.*does\s+not\s+comply\s+with\s+RFC.*/isu", $e->getMessage())) {
                    $output->writeln('<error> Error in email format. Unsubscribing user. </error>');
                    $mailManager->unsubscribeUser($item->getUserId());
                } else {
                    $item->addOneTry();
                    $output->writeln(
                        '<error> Error message: ' . $e->getMessage() . ' </error>'
                    );
                }
            }

            if ($item->getTryCount() > Queue::MAX_TRY_COUNT) {
                $output->writeln('<error> Max send attempts reached. Unsubscribing user. </error>');
                $mailManager->unsubscribeUser($item->getUserId());
            } else {
                $em->flush($item);
            }

        }

        $left = $mailManager->getQueueItemsCount();
        if ($left) {
            $output->writeln('<info>' . $left . ' items left in queue</info>');
        } else {
            $output->writeln('<info>Queue is clean</info>');
        }
    }
}