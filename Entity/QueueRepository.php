<?php

namespace Cleverweb\MailsBundle\Entity;


use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\User;

/**
 * QueueRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class QueueRepository extends EntityRepository
{

    /**
     * return array() with keys: countAll, countSent
     *
     * @param Mailing $mailing
     * @return array
     */
    public function getStatus(Mailing $mailing)
    {
        $countAll = $this->createQueryBuilder('q')
            ->select('COUNT(q)')
            ->where('q.mailing = :mailing')
            ->setParameter('mailing', $mailing)
            ->getQuery()
            ->getSingleScalarResult();
        $countSent = $this->createQueryBuilder('q')
            ->select('COUNT(q)')
            ->where('q.mailing = :mailing')
            ->andWhere('q.sent = 1')
            ->setParameter('mailing', $mailing)
            ->getQuery()
            ->getSingleScalarResult();

        return array('countAll' => $countAll, 'countSent' => $countSent);
    }

    /**
     *
     *
     * @param $maxCount
     * @return Queue[]
     */
    public function getQueueItemsForSend($maxCount)
    {
        return $this->createQueryBuilder('q')
            ->join('q.mailing', 'm')
            ->where('q.sent = 0')
            ->andWhere('m.enabled = 1')
            ->orderBy('q.id', 'ASC')
            ->getQuery()
            ->setMaxResults($maxCount)
            ->getResult();
    }

    /**
     *
     * @return integer
     */
    public function getQueueItemsForSendCount()
    {
        return $this->createQueryBuilder('q')
            ->select('COUNT(q)')
            ->join('q.mailing', 'm')
            ->where('q.sent = 0')
            ->andWhere('m.enabled = 1')
            ->orderBy('q.id', 'ASC')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function removeUserFromQueue(User $user)
    {
        return $this->createQueryBuilder('q')
            ->delete()
            ->where('q.userId = :id')
            ->setParameter('id', $user->getId())
            ->getQuery()
            ->execute();
    }
}
