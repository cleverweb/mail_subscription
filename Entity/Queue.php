<?php

namespace Cleverweb\MailsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Cleverweb\MailsBundle\Entity\Mailing;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Queue
 *
 * @ORM\Table(name="queue")
 * @ORM\Entity(repositoryClass="Cleverweb\MailsBundle\Entity\QueueRepository")
 */
class Queue
{
    const MAX_TRY_COUNT = 10;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Mailing
     *
     * @ORM\ManyToOne(targetEntity="Mailing", inversedBy="queues")
     * @ORM\JoinColumn(name="mailing_id", referencedColumnName="id")
     */
    protected $mailing;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    protected $userId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sent", type="boolean")
     */
    protected $sent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sentAt", type="datetime")
     */
    protected $sentAt;

    /**
     * @ORM\Column(name="try_count", type="integer", nullable=true)
     */
    protected $tryCount;


    public function __toString()
    {
        $name = strval($this->id);
        if ($this->mailing != null) {
            $name .= ':' . $this->mailing->getName();
        }

        return $name;
    }

    public function __construct()
    {
        $this->sent = false;
        $this->sentAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Queue
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set sent
     *
     * @param boolean $sent
     * @return Queue
     */
    public function setSent($sent)
    {
        if (null == $sent) {
            $sent = false;
        }
        $this->sent = $sent;

        return $this;
    }

    /**
     * Get sent
     *
     * @return boolean
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     * @return Queue
     */
    public function setSentAt($sentAt)
    {
        if (null == $sentAt) {
            $sentAt = new \DateTime();
        }
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Set mailing
     *
     * @param Mailing $mailing
     * @return Queue
     */
    public function setMailing(Mailing $mailing = null)
    {
        $this->mailing = $mailing;

        return $this;
    }

    /**
     * Get mailing
     *
     * @return Mailing
     */
    public function getMailing()
    {
        return $this->mailing;
    }


    /**
     * Set userId
     *
     * @param integer $userId
     * @return Queue
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param integer $tryCount
     * @return $this
     */
    public function setTryCount($tryCount)
    {
        $this->tryCount = $tryCount;

        return $this;
    }

    public function addOneTry()
    {
        $this->tryCount += 1;

        return $this->tryCount;
    }

    /**
     * @return integer
     */
    public function getTryCount()
    {
        return $this->tryCount;
    }


}