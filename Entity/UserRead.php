<?php

namespace Cleverweb\MailsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Cleverweb\MailsBundle\Entity\Mailing;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * "User read letter" record
 *
 * @ORM\Table(name="user_read")
 * @ORM\Entity(repositoryClass="Cleverweb\MailsBundle\Entity\UserReadRepository")
 */
class UserRead
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Letter
     *
     * @ORM\ManyToOne(targetEntity="Letter", inversedBy="userReads")
     * @ORM\JoinColumn(name="letter_id", referencedColumnName="id")
     */
    protected $letter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="read_at", type="datetime", nullable=true)
     */
    protected $readAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    protected $userId;


    public function __toString()
    {
        $name = strval($this->id);
        if ($this->mailing != null) {
            $name .= ':' . $this->mailing->getName();
        }

        return $name;
    }

    public function __construct()
    {
        $this->readAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getReadAt()
    {
        return $this->readAt;
    }

    /**
     * @param \DateTime $readAt
     */
    public function setReadAt($readAt)
    {
        $this->readAt = $readAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param \DateTime $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return Letter
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * @param Letter $letter
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;

        return $this;
    }

}