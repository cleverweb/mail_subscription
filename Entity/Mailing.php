<?php

namespace Cleverweb\MailsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Cleverweb\MailsBundle\Entity\Queue;
use Cleverweb\MailsBundle\Entity\Letter;

/**
 * Mailing
 *
 * @ORM\Table(name="mailing")
 * @ORM\Entity(repositoryClass="Cleverweb\MailsBundle\Entity\MailingRepository")
 */
class Mailing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Letter", inversedBy="mailings")
     * @ORM\JoinColumn(name="letter_id", referencedColumnName="id")
     */
    protected $letter;

    /**
     * @ORM\OneToMany(targetEntity="Queue", mappedBy="mailing", cascade={"all"})
     */
    protected $queues;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    protected $startDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @ORM\Column(name="queue_created", type="boolean")
     */
    protected $queueCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="filter", type="string", length=255, nullable=true)
     */
    protected $filter;

    /**
     * @var integer
     *
     * @ORM\Column(name="read_count", type="integer", nullable=true)
     */
    protected $readCount;


    public function __toString()
    {
        return $this->id . ':' . $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Mailing
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Mailing
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Mailing
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->queues = new ArrayCollection();
        $this->queueCreated = false;
        $this->readCount = 0;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Mailing
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set letter
     *
     * @param Letter $letter
     * @return Mailing
     */
    public function setLetter(Letter $letter = null)
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * Get letter
     *
     * @return Letter
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Add queues
     *
     * @param Queue $queue
     * @return Mailing
     */
    public function addQueue(Queue $queue)
    {
        $this->queues[] = $queue;

        return $this;
    }

    /**
     * Remove queues
     *
     * @param Queue $queue
     */
    public function removeQueue(Queue $queue)
    {
        $this->queues->removeElement($queue);
    }

    /**
     * Get queues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQueues()
    {
        return $this->queues;
    }

    /**
     * Set queueCreated
     *
     * @param boolean $queueCreated
     * @return Mailing
     */
    public function setQueueCreated($queueCreated)
    {
        $this->queueCreated = $queueCreated;

        return $this;
    }

    /**
     * Get queueCreated
     *
     * @return boolean
     */
    public function getQueueCreated()
    {
        return $this->queueCreated;
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function setFilter($filter)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @return string
     */
    public function getReadCount()
    {
        return $this->readCount;
    }

    /**
     * @param string $readCount
     */
    public function setReadCount($readCount)
    {
        $this->readCount = $readCount;

        return $this;
    }

    public function increaseReadCount($count = 1)
    {
        $this->readCount += $count;

        return $this;
    }

}