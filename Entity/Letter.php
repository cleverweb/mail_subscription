<?php

namespace Cleverweb\MailsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Cleverweb\MailsBundle\Entity\Mailing;
use Doctrine\Common\Collections\Collection;

/**
 * Letter
 *
 * @ORM\Table(name="letter")
 * @ORM\Entity(repositoryClass="Cleverweb\MailsBundle\Entity\LetterRepository")
 */
class Letter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Mailing", mappedBy="letter", cascade={"all"})
     */
    protected $mailings;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="html", type="text")
     */
    protected $html;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255)
     */
    protected $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="from_field", type="string", length=255)
     */
    protected $from;

    /**
     * @var integer
     *
     * @ORM\Column(name="read_count", type="integer", nullable=true)
     */
    protected $readCount;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserRead", mappedBy="letter")
     */
    protected $userReads;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Letter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set html
     *
     * @param string $html
     * @return Letter
     */
    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Get html
     *
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Letter
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mailings = new ArrayCollection();
        $this->readCount = 0;
    }

    /**
     * Add mailings
     *
     * @param Mailing $mailings
     * @return Letter
     */
    public function addMailing(Mailing $mailings)
    {
        $this->mailings[] = $mailings;

        return $this;
    }

    /**
     * Remove mailings
     *
     * @param Mailing $mailings
     */
    public function removeMailing(Mailing $mailings)
    {
        $this->mailings->removeElement($mailings);
    }

    /**
     * Get mailings
     *
     * @return Collection
     */
    public function getMailings()
    {
        return $this->mailings;
    }

    public function __toString()
    {
        return strval($this->getName());
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Letter
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set from
     *
     * @param string $from
     * @return Letter
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getReadCount()
    {
        return $this->readCount;
    }

    /**
     * @param string $readCount
     */
    public function setReadCount($readCount)
    {
        $this->readCount = $readCount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserReads()
    {
        return $this->userReads;
    }

    public function setUserReads($userReads)
    {
        $this->userReads = $userReads;

        return $this;
    }

    public function addUserReads(UserRead $userRead)
    {
        $this->userReads->add($userRead);

        return $this;
    }

    public function removeUserReads(UserRead $userRead)
    {
        $this->userReads->removeElement($userRead);

        return $this;
    }

    public function increaseReadCount($count = 1)
    {
        $this->readCount += $count;

        return $this;
    }
}