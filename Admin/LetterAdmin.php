<?php

namespace Cleverweb\MailsBundle\Admin;


use Cleverweb\MailsBundle\Entity\Letter;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class LetterAdmin extends Admin
{

    protected $baseRouteName = 'admin_letter';

    protected $baseRoutePattern = 'letter';

    protected $formOptions = array('validation_groups' => array('AdminLetter'));

    protected $maxPerPage = 25;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('label' => 'letter.name'))
            ->add('from', null, array('label' => 'letter.from'))
            ->add('subject', null, array('label' => 'letter.subject'))
            ->add('html', 'ckeditor', array('label' => 'letter.html'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array('label' => 'letter.name'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, array('label' => 'letter.name'))
            ->add('readCount', null, array('label' => 'letter.read_count'))
        ;
    }


    /**
     * @param Letter $letter
     * @return mixed|void
     */
    public function preUpdate($letter)
    {
        $letter->setUpdatedAt(new \DateTime());
    }

    /**
     * @param Letter $letter
     * @return mixed|void
     */
    public function prePersist($letter)
    {
        $letter->setUpdatedAt(new \DateTime());
    }

}