<?php

namespace Cleverweb\MailsBundle\Admin;


use Cleverweb\MailsBundle\Entity\Queue;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class QueueAdmin extends Admin
{

    protected $baseRouteName = 'admin_queue';

    protected $baseRoutePattern = 'queue';

    protected $formOptions = array('validation_groups' => array('AdminQueue'));

    protected $maxPerPage = 25;

    public function getNewInstance()
    {
        /**
         * @var Queue $instance
         */
        $instance = parent::getNewInstance();
        $instance->setSent(false);
        $instance->setSentAt(new \DateTime());
        return $instance;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('mailing', 'sonata_type_model', array('label' => 'queue.mailing'))
            ->add('email', null, array('label' => 'queue.email'))//            ->add('sent')
            //            ->add('sentAt')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('mailing', null, array('label' => 'queue.mailing'))
            ->add('email', null, array('label' => 'queue.email'))
            ->add('sent', null, array('label' => 'queue.sent'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'queue.id'))
            ->addIdentifier('mailing.name', null, array('label' => 'queue.mailing'))
            ->addIdentifier('sent', null, array('label' => 'queue.sent'));
    }
}