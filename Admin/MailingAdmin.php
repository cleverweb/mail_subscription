<?php

namespace Cleverweb\MailsBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Cleverweb\MailsBundle\Entity\Mailing;
use Cleverweb\MailsBundle\Service\MailingManager;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MailingAdmin extends Admin
{

    protected $baseRouteName = 'admin_mailing';

    protected $baseRoutePattern = 'mailing';

    protected $formOptions = array('validation_groups' => array('AdminMailing'));

    protected $translationDomain = 'CleverwebMailsBundle';

    protected $userFilters = array();

    protected $maxPerPage = 25;

    /** @var MailingManager */
    protected $mailingManager;

    public function getNewInstance()
    {
        /** @var Mailing $instance */
        $instance = parent::getNewInstance();
        $instance->setStartDate(new \DateTime());
        $instance->setQueueCreated(false);
        $instance->setEnabled(false);

        $filters = array_keys($this->userFilters);
        if ($this->userFilters) $instance->setFilter($filters[0]);

        return $instance;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('label' => 'mailing.name'))
            ->add('letter', null, array('label' => 'mailing.letter'))
            ->add('startDate', null, array('label' => 'mailing.start_date'))
            ->add('enabled', null, array('label' => 'mailing.enabled', 'required' => false));
        if ($this->userFilters) {
            $formMapper->add('filter', 'choice', array(
                'choices' => $this->userFilters,
                'required' => false,
                'label' => 'mailing.filter.title',
                'empty_value' => false,
            ));
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('letter', null, array('label' => 'mailing.letter'))
            ->add('startDate', null, array('label' => 'mailing.start_date'))
            ->add('enabled', null, array('label' => 'mailing.enabled'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, array('label' => 'mailing.name'))
            ->add('letter', null, array('label' => 'mailing.letter'))
            ->add('startDate', null, array('label' => 'mailing.start_date'))
            ->add('enabled', null, array('label' => 'mailing.enabled'))
            ->add('readCount', null, array('label' => 'mailing.read_count'))
        ;
        if ($this->userFilters) {
            $listMapper->add('_userFilters', null, array(
                'label' => 'mailing.filter.title',
                'template' => 'CleverwebMailsBundle:MailingAdmin:list_userFilters.html.twig'
            ));
        }
        $listMapper->
            add('_action', 'actions', array(
                'actions' => array(
                    'turnon' => array('template' => 'CleverwebMailsBundle:Admin:list__action_turnon.html.twig'),
                    'turnoff' => array('template' => 'CleverwebMailsBundle:Admin:list__action_turnoff.html.twig'),
                )
            ));
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('turnon');
        $collection->add('turnoff');
    }

    public function getBatchActions()
    {
        // retrieve the default (currently only the delete action) actions
        $actions = parent::getBatchActions();

        // check user permissions
        if ($this->hasRoute('turnon')) {
            $actions['turnon'] = array(
                'label' => /** @Ignore */
                $this->trans('action_turnon', array(), 'CleverwebMailsBundle'),
                'ask_confirmation' => true // If true, a confirmation will be asked before performing the action
            );
            $actions['turnoff'] = array(
                'label' => /** @Ignore */
                $this->trans('action_turnoff', array(), 'CleverwebMailsBundle'),
                'ask_confirmation' => true // If true, a confirmation will be asked before performing the action
            );

        }

        return $actions;
    }

    /**
     * @param Mailing $mailing
     * @return mixed|void
     */
    public function preUpdate($mailing)
    {
        $mailing->setUpdatedAt(new \DateTime());
        if ($mailing->getEnabled()) {
            if (count($mailing->getQueues()) == 0) {
                $this->mailingManager->createQueue($mailing);
            }
        }
    }

    /**
     * @param Mailing $mailing
     * @return mixed|void
     */
    public function prePersist($mailing)
    {
        $mailing->setUpdatedAt(new \DateTime());
    }

    /**
     * @param Mailing $mailing
     * @return mixed|void
     */
    public function postPersist($mailing)
    {
        if ($mailing->getEnabled()) {
            $this->mailingManager->createQueue($mailing);
        }
    }

    public function setUserFilters($userConfig)
    {
        foreach ($userConfig['filters'] as $name => $f) {
            if ($name == 'all') {
                $this->userFilters[''] = $this->trans('mailing.filter.all', array(), 'CleverwebMailsBundle');
            } else {
                $this->userFilters[$name] = /** @Ignore */
                    $this->trans('mailing.filter.' . $name, array(), 'CleverwebMailsBundle');
            }
        }
    }

    public function setMailingManager(MailingManager $mailingManager)
    {
        $this->mailingManager = $mailingManager;
    }
}