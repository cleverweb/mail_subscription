<?php


namespace Cleverweb\MailsBundle\Block;

use Cleverweb\MailsBundle\Service\MailingManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;

use Sonata\BlockBundle\Block\BaseBlockService;


class QueueManagerBlockService extends BaseBlockService
{

    protected $mailingManager;

    public function __construct($name, $templating, MailingManager $mailingManager)
    {
        parent::__construct($name, $templating);
        $this->mailingManager = $mailingManager;
    }

    function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'url' => false,
            'title' => 'dashboard.queue_manager_block',
            'template' => 'CleverwebMailsBundle:Block:queue_manager.html.twig',
        ));
    }

    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {

    }

    function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {

    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $activeMailings = $this->mailingManager->getActiveMailings();

        return $this->renderResponse($blockContext->getTemplate(), array(
            'active_mailings' => $activeMailings,
            'block' => $blockContext->getBlock(),
            'settings' => $blockContext->getSettings(),
        ));

    }


}