<?php

namespace Cleverweb\MailsBundle\Translator\Extractor;

use JMS\TranslationBundle\Model\Message;
use JMS\TranslationBundle\Model\MessageCatalogue;
use JMS\TranslationBundle\Translation\ExtractorInterface;

class TranslationExtractor implements ExtractorInterface
{

    private $userConfig;

    protected $catalogue;

    public function __construct($userConfig)
    {
        $this->userConfig = $userConfig;
    }

    public function extract()
    {
        if ($this->catalogue) {
            throw new \RuntimeException('Invalid state');
        }

        $domain = 'CleverwebMailsBundle';

        $this->catalogue = new MessageCatalogue;

        $this->catalogue = new MessageCatalogue();

        $label = 'mailing.filter.all';
        $message = new Message($label, $domain);
        $this->catalogue->add($message);

        foreach ($this->userConfig['filters'] as $name => $filter) {
            $label = 'mailing.filter.' . $name;
            $message = new Message($label, $domain);
            $this->catalogue->add($message);
        }

        $catalogue = $this->catalogue;
        $this->catalogue = false;

        return $catalogue;
    }
}