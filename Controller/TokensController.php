<?php

namespace Cleverweb\MailsBundle\Controller;

use Cleverweb\MailsBundle\CleverwebMailsEvents;
use Cleverweb\MailsBundle\Event\EmailTokensEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TokensController extends Controller
{
    /**
     * @Template
     */
    public function getTokensAction()
    {
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new EmailTokensEvent(null, null);
        $eventDispatcher->dispatch(CleverwebMailsEvents::ON_GET_LETTER_VARIABLES, $event);

        $tokens = $event->getTokens() + $this->get('cleverweb.mailing_manager')->getStandardTokens(null);

        return ['tokens' => array_keys($tokens)];
    }
}
