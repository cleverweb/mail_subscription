<?php

namespace Cleverweb\MailsBundle\Controller;

use Cleverweb\MailsBundle\Entity\Mailing;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/qm_admin")
 */
class BlockController extends Controller
{
    /**
     * @Route("/clean_queue/{id}", name="qm_admin_stop_queue", requirements={"id" = "\d+"})
     * @Template()
     */
    public function cleanQueueAction($id)
    {
        $mailManager = $this->get('cleverweb.mailing_manager');
        $mailManager->stopQueue($id);

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    /**
     * @Route("/restart_queue/{id}", name="qm_admin_restart_queue", requirements={"id" = "\d+"})
     * @Template()
     */
    public function restartQueueAction($id)
    {
        $mailManager = $this->get('cleverweb.mailing_manager');

        $mailManager->clearQueue($id);
        $mailing = $mailManager->getMailingById($id);
        $mailManager->createQueue($mailing);

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    /**
     * @Route("/stop_and_clean/{id}", name="qm_admin_stop_and_clean", requirements={"id" = "\d+"})
     * @Template()
     */
    public function stopAndCleanAction($id)
    {
        $mailManager = $this->get('cleverweb.mailing_manager');

        $mailManager->clearQueue($id, true);

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }
}