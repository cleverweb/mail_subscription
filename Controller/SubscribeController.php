<?php

namespace Cleverweb\MailsBundle\Controller;

use Cleverweb\MailsBundle\Entity\Letter;
use Cleverweb\MailsBundle\Entity\Mailing;
use Cleverweb\MailsBundle\Entity\UserRead;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * @Route("/")
 */
class SubscribeController extends Controller
{
    /**
     * @Route("/subscribe/deactivate/{userId}/{userCode}", name="subscribe_deactivate", requirements={"userId" = "\d+", "userCode" = ".*"})
     * @Template()
     */
    public function deactivateAction($userId, $userCode)
    {
        $mm = $this->get('cleverweb.mailing_manager');
        $result = $mm->checkAndUnsubscribeUser($userId, $userCode);

        return array('result' => $result);
    }

    /**
     * @Route("/images/spacer_{linkData}", name="subscribe_email_was_read")
     */
    public function readEmailAction($linkData)
    {
        $linkGenerator = $this->get('cleverweb.link_generator');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $data = $linkGenerator->decodeLinkData($linkData);

        /** @var Letter $letter */
        $letter = $data['letter'];

        if ($letter) {

            /** @var UserRead[] $userReads */
            $userReads = $em->getRepository('CleverwebMailsBundle:UserRead')->findBy([
                    'letter' => $letter,
                    'userId' => $data['userId']
                ]);

            if (count($userReads) == 0) {
                /** @var Mailing $mailing */
                $mailing = $data['mailing'];
                if ($mailing) {
                    $mailing->increaseReadCount();
                }
                $letter->increaseReadCount();

                $userRead = new UserRead();
                $userRead
                    ->setUserId($data['userId'])
                    ->setReadAt(new \DateTime())
                    ->setLetter($letter);
                $em->persist($userRead);
                $em->flush();
            }
        }

        $path = __DIR__ . '/../Resources/images/spacer.gif';
        return new BinaryFileResponse($path);
    }
}
