<?php

namespace Cleverweb\MailsBundle\Controller;

use Cleverweb\MailsBundle\Entity\Mailing;
use Sonata\AdminBundle\Controller\CRUDController as SonataCRUD;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AdminController extends SonataCRUD
{

    public function turnonAction()
    {
        $id = $this->container->get('request')->get('id');

        if ($this->admin->isGranted('EDIT') === false) {
            throw new AccessDeniedException();
        }

        $mailing = $this->admin->getObject($id);
        $mailing->setEnabled(true);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $mailingManager = $this->get('cleverweb.mailing_manager');
        if (count($mailing->getQueues()) == 0) {
            $mailingManager->createQueue($mailing);
        }

        $this->addFlash(
            'sonata_flash_success',
            $this->container->get('translator')->trans('mailing_turned_on', array(), 'CleverwebMailsBundle')
        );

        return $this->redirect($this->generateUrl('admin_mailing_list'));
    }

    public function turnoffAction()
    {
        $id = $this->container->get('request')->get('id');

        if ($this->admin->isGranted('EDIT') === false) {
            throw new AccessDeniedException();
        }
        $mailing = $this->admin->getObject($id);
        $mailing->setEnabled(false);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $this->addFlash(
            'sonata_flash_success',
            $this->container->get('translator')->trans('mailing_turned_off', array(), 'CleverwebMailsBundle')
        );

        return $this->redirect($this->generateUrl('admin_mailing_list'));
    }

    public function batchActionTurnoff(ProxyQuery $selectedModelQuery)
    {
        $request = $this->get('request');
        $modelManager = $this->admin->getModelManager();

        /** @var Mailing[] $selectedModels */
        $selectedModels = $selectedModelQuery->execute();

        try {
            foreach ($selectedModels as $selectedModel) {

                $selectedModel->setEnabled(false);
                $modelManager->update($selectedModel);
            }
        } catch (\Exception $e) {
            $this->addFlash(
                'sonata_flash_success',
                $this->container->get('translator')->trans('batch_error_mailing_turned_off', array(), 'CleverwebMailsBundle')
            );

            return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
        }
        $this->addFlash(
            'sonata_flash_success',
            $this->container->get('translator')->trans('batch_mailing_turned_off', array(), 'CleverwebMailsBundle')
        );

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

    public function batchActionTurnon(ProxyQuery $selectedModelQuery)
    {
        $request = $this->get('request');
        $mailingManager = $this->get('cleverweb.mailing_manager');
        $modelManager = $this->admin->getModelManager();

        /** @var Mailing[] $selectedModels */
        $selectedModels = $selectedModelQuery->execute();

        try {
            foreach ($selectedModels as $selectedModel) {

                $selectedModel->setEnabled(true);
                $modelManager->update($selectedModel);
                if (count($selectedModel->getQueues()) == 0) {
                    $mailingManager->createQueue($selectedModel);
                }
            }
        } catch (\Exception $e) {
            $this->addFlash(
                'sonata_flash_error',
                $this->container->get('translator')->trans('batch_error_mailing_turned_on', array(), 'CleverwebMailsBundle')
            );


            return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
        }
        $this->addFlash(
            'sonata_flash_success',
            $this->container->get('translator')->trans('batch_mailing_turned_on', array(), 'CleverwebMailsBundle')
        );

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }
}